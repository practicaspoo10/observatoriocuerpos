package ito.poo.app;

import ito.poo.clases.CuerposCelestes;
import ito.poo.clases.Ubicacion;

public class MyApp {
	
			static void run() {
				CuerposCelestes cc1= new CuerposCelestes("Sol", null , "Solidos");
				System.out.println (cc1);
				System.out.println ();

				Ubicacion u1 = new Ubicacion("42 horas",  412.2676200f, (float) 1.97331400, 954.215F);
				System.out.println (u1);
				cc1.desplazamiento(0, 0);
				System.out.println ();
				
				System.out.println ();
				System.out.println ("/*********************/");
				System.out.println ();
				
				CuerposCelestes cc2= new CuerposCelestes("Estrellas",null, "Gas");
				System.out.println (cc2);
				System.out.println ();

				Ubicacion u2 = new Ubicacion("7 meses", 21.87305556f, (float) 57.22333333, 20.4970F);
				System.out.println (u2);
				cc2.desplazamiento(1, 789);
				
				System.out.println ();
				System.out.println ("/*********************/");
				System.out.println ();
				
				
				
				System.out.println(!cc1.equals(cc2));
			    System.out.println(cc2.compareTo(cc1));
			    
				System.out.println ();

			    System.out.println(!u1.equals(u2));
			    System.out.println(u2.compareTo(u1));
			    
			    System.out.println ();
			    
			}
			
			public static void main(String[] args) {
				run();
			}
		}


	