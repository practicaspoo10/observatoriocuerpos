package ito.poo.clases;

import java.util.HashSet;

public class CuerposCelestes {
	
			private String nomb=  " ";
			private HashSet<Ubicacion> localizacion;
			private String composic=  " ";
			
			public CuerposCelestes() {
				super();
			}
			
			public CuerposCelestes(String nomb, HashSet<Ubicacion> localizacion, String composic) {
				super();
				this.nomb = nomb;
				this.localizacion = localizacion;
				this.composic = composic;
			}
			
			public float desplazamiento(int i, int j  ) {	
			float desplazamiento=0f;
			desplazamiento=(float) (Math.sqrt(Math.pow(i,2)+Math.pow(j, 2)));
			if(desplazamiento==0)
				System.out.println("-1");
			else
				System.out.println("El desplazamiento es de:"+desplazamiento);
					return (float) desplazamiento;
			}

			public float desplazamiento(int j ) {
			float  desplazamiento=j;
			return desplazamiento;
			}

			public HashSet<Ubicacion> getLocalizacion() {
				return this.localizacion;
			}

			public void setLocalizacion(HashSet<Ubicacion> localizacion) {
				this.localizacion = localizacion;
			}

			public String getComposic() {
				return composic;
			}

			public void setComposic(String composic) {
				this.composic = composic;
			}

			public String getNomb() {
				return nomb;
			}
			public String toString() {
				return "CuerposCelestes [nomb=" + nomb + ", localizacion=" + localizacion + ", composicion=" + composic + "]";
			}

			@Override
			public int hashCode() {
				final int prime = 31;
				int result = 1;
				result = prime * result + ((composic == null) ? 0 : composic.hashCode());
				result = prime * result + ((localizacion == null) ? 0 : localizacion.hashCode());
				result = prime * result + ((nomb == null) ? 0 : nomb.hashCode());
				return result;
			}
			
			@Override
			public boolean equals(Object obj) {
				if (this == obj)
					return true;
				if (obj == null)
					return false;
				if (getClass() != obj.getClass())
					return false;
				CuerposCelestes other = (CuerposCelestes) obj;
				if (composic == null) {
					if (other.composic != null)
						return false;
				} else if (!composic.equals(other.composic))
					return false;
				if (localizacion == null) {
					if (other.localizacion != null)
						return false;
				} else if (!localizacion.equals(other.localizacion))
					return false;
				if (nomb == null) {
					if (other.nomb != null)
						return false;
				} else if (!nomb.equals(other.nomb))
					return false;
				return true;
			}
			public int compareTo(CuerposCelestes arg0) {
				int r = 0;
				if (!this.nomb.equals(arg0.getNomb()))
					return this.nomb.compareTo(arg0.getNomb());
				else if (!this.composic.equals(arg0.getComposic()))
					return this.composic.compareTo(arg0.getComposic());
				return r;
			}	
		}



