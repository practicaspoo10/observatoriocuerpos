package ito.poo.clases;

import java.util.HashSet;

public class Ubicacion {
	
		private String periodo=  " ";
		private float dist= 0F;;
		private float longi= 0F;;
		private float latit= 0F;;
		private HashSet<Ubicacion> localizacion;

		public Ubicacion() {
			super();
		}
		
		public Ubicacion(String periodo, float dist, float longi, float latit) {
			super();
			this.periodo = periodo;
			this.dist = dist;
			this.longi = longi;
			this.latit = latit;
		}

		public float getDist() {
			return dist;
		}

		public void setDistancia(float dist) {
			this.dist = dist;
		}

		public float getLongi() {
			return this.longi;
		}

		public float setLongi() {
			return this.longi;
		}

		public float getLatit() {
			return this.latit;
		}
		
		public float setLatit() {
			return this.latit;
		}

		public String getPeriodo() {
			return periodo;
		}
		
		public void desplazamiento(int i, int j) {		
		}

		public String toString() {
			return "Ubicacion [periodo=" + periodo + ", dist= " + dist + ", longi=" + longi + ", latit=" + latit + "]";
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + Float.floatToIntBits(dist);
			result = prime * result + Float.floatToIntBits(latit);
			result = prime * result + ((localizacion == null) ? 0 : localizacion.hashCode());
			result = prime * result + Float.floatToIntBits(longi);
			result = prime * result + ((periodo == null) ? 0 : periodo.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Ubicacion other = (Ubicacion) obj;
			if (Float.floatToIntBits(dist) != Float.floatToIntBits(other.dist))
				return false;
			if (Float.floatToIntBits(latit) != Float.floatToIntBits(other.latit))
				return false;
			if (localizacion == null) {
				if (other.localizacion != null)
					return false;
			} else if (!localizacion.equals(other.localizacion))
				return false;
			if (Float.floatToIntBits(longi) != Float.floatToIntBits(other.longi))
				return false;
			if (periodo == null) {
				if (other.periodo != null)
					return false;
			} else if (!periodo.equals(other.periodo))
				return false;
			return true;
		}
		public int compareTo(Ubicacion arg0) {
			int r = 0;
			if (!this.periodo.equals(arg0.getPeriodo()))
				return this.periodo.compareTo(arg0.getPeriodo());
			else if (this.dist != arg0.getDist())
				return this.dist > arg0.getDist() ? 1 : -1;
			else if (this.latit != arg0.getLatit())
				return this.latit > arg0.getLatit() ? 2 : -2;
			else if (this.longi != arg0.getLongi())
				return this.longi > arg0.getLongi() ? 3 : -3;
			return r;
		}
	}


